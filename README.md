# SIALAC Benchmark

Python generator for MATSim plans.

## Setup

Use `venv` (https://gitlab.com/juliendehos/venv)

## Prerequisites

MATSim network files (`.xml`)
*(test files are available in `input/` folder)*

## Install

```sh
venv i sialac_benchmark
venv a sialac_benchmark
pip install pkgconfig setuptools
pip install . --upgrade
```

## Usage

### Run

```sh
sialac_benchmark_cli.py [ params ] > out_plans.xml
```

### Params
- `np` : number of persons to generate,
- `nw` : network file path,
- `nc` : number of clusters by axis *(e.g. `nc=100` results in `100 x 100 = 10000` clusters)*,
- `hc` *(optional)* : home clusters centres coordinates (format : `"line:column"`).
  <br>
  In case of multiple clusters, separate centres coordinates with `|` symbol
  (`"line_1:col_1|line_2:col_2|...|line_n:col_n"`),
- `hr` *(only if `hc` parameter is specified)* : home clusters radii.
  <br>
  If multiple clusters, separate radii with `|` symbol (`"rad_1|rad_2|...|rad_n"`),
- `hw` *(only if `hc` parameter is specified)* : home clusters weights.
  <br>
  If multiple clusters, separate weights with `|` symbol
  (`"weight_1|weight_2|...|weight_n"`),
- `wc` *(optional)* : work clusters centres coordinates (cf. `hc`),
- `wr` *(only if `wc` parameter is specified)* : work clusters radii (c.f `hr`).
- `ww` *(only if `wc` parameter is specified)* : work clusters weights (c.f `hw`).

## Examples

Generate 5000 persons centered in the network:

```sh
sialac_benchmark_cli.py np=5000,nw=networks/calais.xml,nc=100 > out_plans.xml
```

Generate 10000 persons with home locations spread in 2 clusters of radii 10 and
5, respectively:

```sh
plan_gen_cli.py np=10000,nw=networks/calais.xml,nc=100,\
 hc="50:50|67:17",hr="10|5" > out_plans.xml
```

## Visualize (experimental)

Visualize home clusters only.

### Run

```sh
sialac_visualizer.py <nb_clusters> <network> <plans>
```

### Example

```sh
sialac_visualizer.py 100 networks/calais.xml out_plans.xml
```
