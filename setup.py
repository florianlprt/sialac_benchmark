import pkgconfig
from setuptools import setup

setup(
    name = 'sialac_benchmark',
    version = '0.1',
    scripts = [
        'sialac_benchmark/sialac_benchmark_cli.py',
        'sialac_benchmark/sialac_visualizer.py'
    ],
    packages = ['sialac_benchmark'],
    install_requires = [
        'numpy',
        'lxml',
        'joblib',
        'pandas',
        'utm',
        'matplotlib'
    ],
)
