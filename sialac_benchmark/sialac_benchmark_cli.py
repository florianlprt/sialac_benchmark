#!/usr/bin/env python3
''' Python generator for MATSim plans. '''

import sys
import lxml.etree as etree
import numpy as np
import joblib as jl
import sialac_benchmark.sialac_benchmark as sialac

if __name__ == '__main__':

    # command line arguments
    if len(sys.argv) != 2:
        print('usage:', sys.argv[0], '<params>')
        sys.exit(-1)
    DICT_PARAMS = sialac.parse_params(sys.argv[1])

    NB_CLUSTERS = DICT_PARAMS['nc']
    NB_PERSONS  = DICT_PARAMS['np']
    INPUT_NETWORK = DICT_PARAMS['nw']
    INPUT_HOMES = DICT_PARAMS['hcsv'] if 'hcsv' in DICT_PARAMS else None
    INPUT_WORKS = DICT_PARAMS['wcsv'] if 'wcsv' in DICT_PARAMS else None
    H_CENTERS = DICT_PARAMS['hc'] if 'hc' in DICT_PARAMS else None
    H_RADIUS  = DICT_PARAMS['hr'] if 'hr' in DICT_PARAMS else None
    H_WEIGHTS = DICT_PARAMS['hw'] if 'hw' in DICT_PARAMS else None
    W_CENTERS = DICT_PARAMS['wc'] if 'wc' in DICT_PARAMS else None
    W_RADIUS  = DICT_PARAMS['wr'] if 'wr' in DICT_PARAMS else None
    W_WEIGHTS = DICT_PARAMS['ww'] if 'ww' in DICT_PARAMS else None
    PT_DENSITY  = DICT_PARAMS['pt'] if 'pt' in DICT_PARAMS else .0

    # prepare data
    NODES = sialac.get_nodes(INPUT_NETWORK)
    CLUSTERS = sialac.make_clusters(NB_CLUSTERS, NODES)

    if (H_CENTERS is None and INPUT_HOMES):
        H_CENTERS = sialac.make_centers(INPUT_HOMES, NB_CLUSTERS, NODES)
        H_WEIGHTS = sialac.parse_weights(INPUT_HOMES)
        H_RADIUS = np.full_like(H_WEIGHTS, 5)
    if (W_CENTERS is None and INPUT_WORKS):
        W_CENTERS = sialac.make_centers(INPUT_WORKS, NB_CLUSTERS, NODES)
        W_WEIGHTS = sialac.parse_weights(INPUT_WORKS)
        W_RADIUS = np.full_like(W_WEIGHTS, 5)

    H_DENSITIES = sialac.make_densities(NB_CLUSTERS, H_CENTERS, H_RADIUS, H_WEIGHTS)
    H_DENSITIES = sialac.clean_densities(H_DENSITIES, CLUSTERS)
    W_DENSITIES = sialac.make_densities(NB_CLUSTERS, W_CENTERS, W_RADIUS, W_WEIGHTS)
    W_DENSITIES = sialac.clean_densities(W_DENSITIES, CLUSTERS)

    # make xml
    PERSONS = [sialac.rand_person(NODES, CLUSTERS, H_DENSITIES, W_DENSITIES)
               for _ in range(NB_PERSONS)]
    PLANS = sialac.make_plans(PERSONS, PT_DENSITY)

    # print XML
    print('<?xml version="1.0" ?>')
    print('<!DOCTYPE plans SYSTEM "http://www.matsim.org/files/dtd/plans_v4.dtd">')
    print(etree.tostring(PLANS, pretty_print=True).decode('utf-8'))
