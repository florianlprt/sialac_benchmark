#!/usr/bin/bash

SIALAC="sialac_benchmark_cli.py"

SCENARIO=$1
POPULATION=$2

benchs_usage() {
    echo "usage: ./benchs.sh <scenario> <population>"
}

benchs_quito() {
    OUT="plans_$SCENARIO_$POPULATION"
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=50,hc="25:25",hr="25",hw="1",wc="25:25",wr="7",ww="1",pt=0.3 > ${OUT}_1h_1a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=50,hc="25:25",hr="25",hw="1",wc="17:15|32:32|24:20|30:28",wr="2|2|2|2",ww="1|1|1|1",pt=0.3 > ${OUT}_1h_4a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=50,hc="40:20|18:11|15:26|34:34",hr="10|15|5|12",hw="1|1|1|1",wc="25:25",wr="7",ww="1",pt=0.3 > ${OUT}_4h_1a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=50,hc="40:20|18:11|15:26|34:34",hr="10|15|5|12",hw="1|1|1|1",wc="17:15|32:32|24:20|30:28",wr="2|2|2|2",ww="1|1|1|1",pt=0.3 > ${OUT}_4h_4a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=50,hc="25:25",hr="1000",hw="1",wc="25:25",wr="7",ww="1",pt=0.3 > ${OUT}_uh_1a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=50,hc="25:25",hr="1000",hw="1",wc="17:15|32:32|24:20|30:28",wr="2|2|2|2",ww="1|1|1|1",pt=0.3 > ${OUT}_uh_4a.xml
}

benchs_calais() {
    OUT="plans_$SCENARIO_$POPULATION"
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=100,hc="50:50",hr="50",hw="1",wc="50:50",wr="20",ww="1",pt=0.3 > ${OUT}_1h_1a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=100,hc="50:50",hr="50",hw="1",wc="45:30|70:45|28:74|56:80",wr="5|5|5|5",ww="2|1|2|1",pt=0.3 > ${OUT}_1h_4a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=100,hc="46:36|69:44|50:75|20:75",hr="20|10|20|8",hw="1|1|1|1",wc="50:50",wr="20",ww="1",pt=0.3 > ${OUT}_4h_1a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=100,hc="46:36|69:44|50:75|20:75",hr="20|10|20|8",hw="1|1|1|1",wc="45:30|70:45|28:74|56:80",wr="5|5|5|5",ww="2|1|2|1",pt=0.3 > ${OUT}_4h_4a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=100,hc="50:50",hr="50000",hw="1",wc="50:50",wr="20",ww="1",pt=0.3 > ${OUT}_uh_1a.xml
    $SIALAC nw=networks/$SCENARIO.xml,np=$POPULATION,nc=100,hc="50:50",hr="50000",hw="1",wc="45:30|70:45|28:74|56:80",wr="5|5|5|5",ww="2|1|2|1",pt=0.3 > ${OUT}_uh_4a.xml
}

if [ $# -lt 2 ] ; then
    benchs_usage
    exit 1
fi

case $1 in
    "quito") benchs_quito $1 $2 ;;
    "calais") benchs_calais $1 $2 ;;
    *) benchs_usage ; echo "error: invalid scenario" ;;
esac
